import smtplib

from datetime import datetime

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from credentials import (
    BOT_EMAIL,
    BOT_EMAIL_PASSWD,
)

from config import (
    SMTP_SERVER,
    SMTP_PORT,
    ERROR_LOG_PATH_FILE,
    MSG_PATH_FILE
)


def logging_error(exception):
    error_log = '{} {}\n'.format(datetime.now(), exception)
    with open(ERROR_LOG_PATH_FILE, 'a') as f:
        f.write(error_log)


def make_letter(title, *args):
    msg = MIMEMultipart()
    msg['from'] = BOT_EMAIL
    msg['Subject'] = title
    body = '\n'.join(args)
    html = open(MSG_PATH_FILE, 'r', encoding='utf-8')
    email_sign = html.read()
    html.close()
    msg.attach(MIMEText(body, 'plain', 'utf-8'))
    msg.attach(MIMEText(email_sign, 'html'))
    return msg.as_string()


def send_to_email(email, title, *text):
    try:
        server = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
        server.login(BOT_EMAIL, BOT_EMAIL_PASSWD)
        server.sendmail(BOT_EMAIL, email, make_letter(title, *text))
        server.quit()
    except smtplib.SMTPException as e:
        logging_error(e)

