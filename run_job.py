import schedule
import time

from parser import get_selector_date_time, record_stream

if __name__ == '__main__':
    schedule.every().monday.at('12:00').do(get_selector_date_time)

    schedule.every().friday.at('12:00').do(get_selector_date_time)

    schedule.every().friday.at("14:55").do(record_stream)

    while True:
        schedule.run_pending()
        time.sleep(1)
