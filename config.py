URL = 'http://video.dogm.mos.ru/online/finans-resource-management.html'
STREAM_URL = 'http://10.128.1.132/live3/smil:schresourceclose.smil/playlist.m3u8'
VIDEO_SAVE_PARAMS = '-c copy -bsf:a aac_adtstoasc'
PATH_TO_STORE_FILE = '/samba/private/Селектор/'
SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 465
ERROR_LOG_PATH_FILE = '/Users/teacher/Documents/selector_downloader/static/error.log'
MSG_PATH_FILE = '/Users/teacher/Documents/selector_downloader/static/msg.html'
FFMPEG_CMD = ''
MONTHS = {
    '1': 'Январь',
    '2': 'Февраль',
    '3': 'Март',
    '4': 'Апрель',
    '5': 'Май',
    '6': 'Июнь',
    '7': 'Июль',
    '8': 'Август',
    '9': 'Сентябрь',
    '10': 'Октябрь',
    '11': 'Ноябрь',
    '12': 'Декабрь'
}
