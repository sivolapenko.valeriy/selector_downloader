import os
import subprocess
from requests_html import HTMLSession
from datetime import datetime

from utils import send_to_email, logging_error

from config import (
    URL,
    STREAM_URL,
    VIDEO_SAVE_PARAMS,
    PATH_TO_STORE_FILE,
    MONTHS,
)

from credentials import EMAILS


def kill_process(proc):
    try:
        proc.communicate(timeout=15)
    except subprocess.TimeoutExpired:
        proc.kill()
        proc.communicate()


def exist_or_create_folder(selector_date):
    """Этот метод используется в методе record_stream()
    Проверяем есть ли папка с текущим годом и подпапкой с текущим месяцем.
    mon получает номер месяца. Номер месяца испльзуется как ключ в словаре MONTHS.
    Если папка есть, то проверяем есть ли подпапка с месяцем.
    Если есть, то просто создаем файл с текущим числом. Например: 24.10.2019.mp4
    Если какой-то из папок не существует, то создаем ее, а после записываем в нее файл с текущим числом.
    @selector_date - текущая дата формата дд.мм.гг. 
    """

    day, mon, year = selector_date.split('_')
    os.chdir(PATH_TO_STORE_FILE)
    if not os.path.isdir(PATH_TO_STORE_FILE + year):
        os.makedirs(year + '/' + MONTHS[mon])
        os.chdir(PATH_TO_STORE_FILE + year)
    if not os.path.isdir(PATH_TO_STORE_FILE + year + '/' + MONTHS[mon]):
        os.makedirs(year + '/' + MONTHS[mon])
    os.chdir(PATH_TO_STORE_FILE + year + '/' + MONTHS[mon])


def record_stream():
    """Получаем текущую дату.
    Проверям наличие структуры папок.
    Записываем поток.
    """

    selector_date = datetime.now().strftime('%d_%m_%Y')
    exist_or_create_folder(selector_date)
    ffmgeg_cmd = subprocess.Popen(f'ffmpeg -i {STREAM_URL} {VIDEO_SAVE_PARAMS} selector_{selector_date}.mp4',
                                  shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if ffmgeg_cmd.wait() != 0:
        send_to_email(
            EMAILS[0],
            'Селектор «Комплексная профилактика управленческих ошибок»',
            f'Не удалось начать запись. Ошибка: {ffmgeg_cmd.stderr.read().decode()}'
        )
        kill_process(ffmgeg_cmd)
    else:
        for recipient in EMAILS:
            send_to_email(
                recipient,
                'Селектор «Комплексная профилактика управленческих ошибок»',
                'Запись селектора закончена (Селектор завершился)'
            )
        kill_process(ffmgeg_cmd)


def get_selector_date_time():
    """Проверям дату следующего селектора.
    Получаем контейнер с датой с сайта.
    Высчитываем разницу и отправляем данные секретарю и администратору.
    """
    selector_datetime = 'Не удалось получить дату'
    awaiting_time_txt = ''
    diff = []

    session = HTMLSession()
    r = session.get(URL)
    try:
        selector_datetime = r.html.find('#CDTs', first=True).attrs['data-time'].replace('/', '-')

        video_date_to_datetime_format = datetime.strptime(selector_datetime, '%Y-%m-%d %H:%M:%S')

        now = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
        diff = str(video_date_to_datetime_format - now).split(' ')

        awaiting_time_txt = f'Осталось дней: {diff[0]}, часов: {diff[2]}'
    except IndexError:
        awaiting_time_txt = f'Осталось часов: {diff[0]}'
    except AttributeError:
        selector_datetime = r.html.find('.panel-body', first=True).text
    except Exception as e:
        logging_error(e)

    for recipient in EMAILS:
        send_to_email(
            recipient,
            'Селектор «Комплексная профилактика управленческих ошибок»',
            'Дата следующего селектора: ' + selector_datetime,
            awaiting_time_txt
        )
    session.close()
